FROM openjdk:8-jdk-alpine3.9
ARG TAG
ENV BAR=$TAG
RUN addgroup -S ileet && adduser -S ileet -G ileet && chown -R ileet:ileet /home
COPY target/$TAG.jar /home/ 
USER ileet
WORKDIR /home
EXPOSE 8090
CMD java -jar $BAR.jar

